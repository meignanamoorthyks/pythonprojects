class Hashtable:
    def __init__(self):
        self.Max = 500
        self.arr = [[] for i in range(self.Max)]

    def getHash(self, key):
        hash = 0
        for char in key:
            hash += ord(char)
        return hash % self.Max
    
    def __setitem__(self, key, val):
        hash = self.getHash(key)
        found = False
        for idx, element in enumerate(self.arr[hash]):
            if len(element)==2 and element[0] == key:
                self.arr[hash][idx] = (key, val)
                found = True
        if not found:   
            self.arr[hash].append((key, val))
    
    def __getitem__(self, key):
        index = self.getHash(key)
        return self.arr[index]

if __name__ == '__main__':
    t = Hashtable()
    t["march 6"] = 310
    t["march 7"] = 420
    t["march 8"] = 67
    t["march 17"] = 63457
    print(t.arr)
    #print(hash.getItem('name'))
    



