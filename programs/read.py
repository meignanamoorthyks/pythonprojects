ipsum_content = open("files/ipsum.txt")

# for line in ipsum_content:
#     print(line.rstrip())

#print(ipsum_content.readlines())
def content_filter(lines):
    return '>' not in lines

with open("files/dna_sequence.txt") as dna_sequence:
    lines = dna_sequence.readlines()
    print(list(filter(content_filter,lines)))