import imp
from space.Planet import Planet
from space.calc import planet_mass,planet_vol

neptune = Planet('Neptune',45000,6.5,'Solar system')

neptune_mass = planet_mass(neptune.gravity, neptune.radius)
neptune_vol = planet_vol(neptune.radius)

print(f'{neptune.name} has a mass of {neptune_mass} and a volume of {neptune_vol}')
