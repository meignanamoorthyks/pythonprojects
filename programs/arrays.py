print("Excercise - 1")
print("----------------------")
expenses = [2200, 2350, 2600, 2130, 2190]
print(f'In Feb, how many dollars you spent extra compare to January: {expenses[1] - expenses[0]}')


first_quat_expenses = []
for i in range(len(expenses)):
    if i < 3:
        first_quat_expenses.append(expenses[i])
        # print(expenses[i])
print(f'Total expense in first quater: {sum(first_quat_expenses)}')


found = 'No'
for i in range(len(expenses)):
    if expenses[i] == 2000:
        found = 'Yes'
print(f'if you spent exactly 2000 dollars in any month : {found}')

expenses.append(1980)
print(f'June month exp: {expenses.pop()}')

expenses[3] = expenses[3] - 200
print("Expenses after 200$ return in April:",expenses)

print("----------------------")
print("Excercise - 2")
print("----------------------")

heros=['spider man','thor','hulk','iron man','captain america']

print(f'Length of heores list: {len(heros)}')

heros.append('black panther')
print(f'After adding new hero the hero list is:{heros}')

heros.remove('black panther')
heros.insert(3, 'black panther')
print(f'After corrections the heroes list is:{heros}')

heros[1:3] = ['doctor strange']
print(f'The doctor strange is added in list:{heros}')

print(f'After sorting the heroes list is:{sorted(heros)}')

print("----------------------")
print("Excercise - 3")
print("----------------------")

userInputNumber = int(input("The Max number you want to genrate Odd numbers : "))
i = 0 
oddNumbers = []
for i in range(1, userInputNumber):
    if i % 2 == 1:
        oddNumbers.append(i)
    i+=1
print(f'The odd numbers from 0 to {userInputNumber}\n')
print("----")
for n in range(len(oddNumbers)):
    print(f'{oddNumbers[n]}\n')
