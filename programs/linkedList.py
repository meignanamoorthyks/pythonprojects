class Node:
    def __init__(self, data = None, next = None):
        self.data = data
        self.next = next

class Linkedlist:
    def __init__(self):
        self.head = None
    
    def getLength(self):
        count = 0
        current = self.head
        while current:
            count+=1
            current = current.next
        return count

    def print(self):
        if self.head is not None:
            current = self.head
            llistStr = ''
            while current:
                llistStr += str(current.data)
                if current.next:
                    llistStr += '-->'
                current = current.next
            print(llistStr)
        else:
            print("Linkedlist is empty!")   
    
    def insert_at_beginning(self, data):
        node = Node(data, self.head)
        self.head = node
    
    def insert_at_end(self, data):
        if self.head is None:
            self.head = Node(data, None)
            return
        
        current = self.head
        while current.next:
            current = current.next
        current.next = Node(data, None)
    
    def insert_at(self, index, data):
        if index > 0 or index < self.getLength():
            count = 0
            current = self.head
            while current:
                if count == index - 1: 
                    node = Node(data, current.next)
                    current.next = node
                current = current.next
                count+=1
        elif index == 0:
            self.insert_at_end(data)
        else:
            print("Invalid index")
    
    def remove_at(self, index):
        if index > 0 or index < self.getLength():
            count = 0
            current = self.head
            while current:
                if count == index - 1: 
                    current.next = current.next.next
                current = current.next
                count+=1      
        else:
            print("Invalid index")   

    def remove_by_value(self, data):
        if self.head is None:
            return

        if self.head.data == data:
            self.head = self.head.next
            return

        current = self.head
        while current.next is not None:
            if current.next.data == data:
                current.next = current.next.next
                break
            current = current.next

    def insert_values(self, data_list):
        self.head = None
        for data in data_list:
            self.insert_at_end(data)
    
    def insert_after_value(self, beforeValue, data):
        if self.head == beforeValue:
            current.next = Node(data, current.next.next)

        current = self.head
        while current:
            if current.data == beforeValue: 
                node = Node(data, current.next)
                current.next = node
                break
            current = current.next
        


if __name__ == '__main__':
    ll = Linkedlist()
    # llist.insert_at_beginning(212)
    # llist.insert_at_beginning(313)
    # llist.insert_at_end(2)
    # llist.insert_at_end(35)
    # llist.insert_values(['oreo','archer','bravo','junee','raggy'])
    # llist.insert_at(3, 'bruno')
    # llist.remove_at(3)
    # llist.remove_by_value('junee')
    # print(f'The length of the linked list : {llist.getLength()}')
    # llist.print()
    ll.insert_values(["banana","mango","grapes","orange"])
    ll.print()
    ll.insert_after_value("mango","apple") # insert apple after mango
    ll.print()
    ll.remove_by_value("orange") # remove orange from linked list
    ll.print()
    ll.remove_by_value("figs")
    ll.print()
    ll.remove_by_value("banana")
    ll.remove_by_value("mango")
    ll.remove_by_value("apple")
    ll.remove_by_value("grapes")
    ll.print()
