marks = [85,91,27,32,87,94,95,77]

def remove_failMarks(mark):
    return mark > 35

#filter method
print(list(filter(remove_failMarks,marks)))

#for loop
filtered = []
for mark in marks:
    if mark > 35:
        filtered.append(mark)
print(filtered)

#comprehension method
print(list(mark for mark in marks if mark > 35))