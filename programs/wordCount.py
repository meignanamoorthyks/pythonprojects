words = {}

with open('files/poem.txt') as f:
    for line in f:
        tokens = line.split(' ')
        for token in tokens:
            token = token.replace('\n',"")
            if token not in words:
                words[token] = 1
            else:
                words[token] += 1

print(words)