weather = []

with open('files/nyc_weather.csv') as f:
    for line in f:
        token = line.split(',')
        if token[0] != 'date':
            weather.append(int(token[1].replace('\n', '')))

avg = sum(weather[0:7])/len(weather[0:7])
print(f"Average of weather in 1st week : {avg}")
print(f"Max weather : {max(weather)}")