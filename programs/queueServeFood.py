#ordering and serving food with Thread using queue
from collections import deque
import time
import threading

class Queue:
    def __init__(self):
        self.buffer = deque()
    
    def enqueue(self, val):
        self.buffer.appendleft(val)
        
    def dequeue(self):
        if len(self.buffer)==0:
            print("Queue is empty")
            return

        return self.buffer.pop()
    
    def is_empty(self):
        return len(self.buffer)==0
    
    def size(self):
        return len(self.buffer)
    
    def placeOrder(self, orders):
        for item in orders:
            print("Placing order for:",item)
            self.enqueue(item)
            time.sleep(0.5)
            
            

    def serveOrder(self):
            while True:
                if self.is_empty():
                    return False
                order = self.dequeue()
                print("Now serving: ",order)
                time.sleep(2)
                     
if __name__ == '__main__':
    queue = Queue()
    orders = ['pizza','samosa','pasta','biryani','burger']
    
    T1 = threading.Thread(target = queue.placeOrder, args = (orders,))
    T2 = threading.Thread(target = queue.serveOrder)

    T1.start()
    time.sleep(1)
    T2.start()

    T1.join()
    T2.join()
    
    print(f"done in: {time.time()}")

    

    
