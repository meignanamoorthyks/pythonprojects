#checks if paranthesis in the string are balanced or not
from collections import deque
class Stack:
    def __init__(self):
        self.container = deque()

    def push(self, val):
        self.container.append(val)

    def pop(self):
        return self.container.pop()
    
    def peek(self):
        return self.container[-1]
    
    def is_empty(self):
        return len(self.container) == 0

    def size(self):
        return len(self.container)
    
    def is_match(self,ch1,ch2):
        matchers = {
            ')':'(',
            ']':'[',
            '}':'{'
        }
        return matchers[ch1] == ch2
    
    def is_balanced(self, str):
        for ch in str:
            if ch == "(" or ch == "[" or ch == "{":
                self.push(ch)
            elif ch == ")" or ch == "]" or ch == "}":
                if self.is_empty():
                    return False
                if not self.is_match(ch, self.pop()):
                    return False
            
        return self.size() == 0
            

if __name__ == '__main__':
    stack = Stack()
    print(stack.is_balanced("({a+b})"))
    print(stack.is_balanced("))((a+b}{"))
    print(stack.is_balanced("((a+b))"))
    print(stack.is_balanced("((a+g))"))
    print(stack.is_balanced("))"))
    print(stack.is_balanced("[a+b]*(x+2y)*{gg+kk}"))             