# def members_intro(dictionary):
#     for key,val in dictionary.items():
#         print(f"{key} is {val}")
def member_count(dictionary):
    roles = list(dictionary.values())
    for item in set(roles):
        num = roles.count(item)
        print(f'There are {num} {item}')


wfmt_members = {}
while True:
    name = input('Enter name: ')
    role = input('role?: ')

    wfmt_members[name] = role

    another = input('want to add another? (y/n)')
    if another == 'y':
        continue
    else:
        break 

# members_intro(wfmt_members)
member_count(wfmt_members)