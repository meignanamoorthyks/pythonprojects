#Reverse a string using Stack
from collections import deque
class Stack:
    def __init__(self):
        self.container = deque()
        self.revContainer = deque()
    
    def pushToStack(self, sentence):
        for letter in sentence:
           self.container.append(letter)
        self.reverseStack()

    def reverseStack(self):
        for word in range(len(self.container)):
            lastWord = self.container.pop()
            self.revContainer.append(lastWord)
        
    def printSentnce(self):
        revSentence = ''
        for word in self.revContainer:
            revSentence += word+' '
        print(revSentence)

if __name__ == '__main__':
    sentence = "We will conquere COVID-19"
    stack = Stack()
    stack.pushToStack(sentence)
    stack.printSentnce()


