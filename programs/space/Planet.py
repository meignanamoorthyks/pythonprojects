class Planet:

    shape = 'round'

    def __init__(self, name, radius, gravity, system):
        self.name = name
        self.radius = radius
        self.gravity = gravity
        self.system = system

    def orbit(self):
        return f'{self.name} is one of the planet in {self.system}'

    @classmethod
    def commons(this):
        return f'All the planet are {this.shape} in shape because of gravity'
    
    @staticmethod
    def rotation(speed = '2000 miles per hour'):
        return f'the planet are rotating in the speed of {speed}'


