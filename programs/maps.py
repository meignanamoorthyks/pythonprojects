from random import shuffle

def jumble(word):
    anagram = list(word)
    shuffle(anagram)
    return '-'.join(anagram)


words = ['oreo','lulu','bravo']

#shuffle using for loop
shuffled = []
for word in words:
    shuffled.append(jumble(word))

print("shuffle using for loop")
print(shuffled)

#shuffle using map function
print("shuffle using map function")
print(list(map(jumble, words)))

#shuffle using comprehension
print("shuffle using comprehension")
print(list(jumble(word) for word in words))

