from random import randint

animals = [
    'dog', 'cat', 'monkey', 'donkey', 'cow', 'buffalo', 'ox',
    'snake', 'elephant', 'crocodile', 'lion', 'tiger', 'cheetah', 'rhinos'
]

def modify(content):
    randomAnimalIndex = randint(0,len(animals) - 1)
    return f'{content} {animals[randomAnimalIndex]}'

paraCount = int(input("How many paragraphs needs to generate : "))

with open('util/ipsum.txt') as ipsum_content:
    word = ipsum_content.read().split()

    for n in range(paraCount):
        modified_content = list(map(modify,word))
        with open('ipsum.txt', 'a') as animal_ipsum:
            animal_ipsum.write(' '.join(modified_content) + '\n\n')