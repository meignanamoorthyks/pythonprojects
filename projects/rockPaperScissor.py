import random

def play():
    userChoice = input("what's your choice? 'r' for Rock 'p' for paper & 's' for Scissor : ")
    printChoice('Your',userChoice)

    computerChoice = random.choice(['r','p','s'])
    printChoice('Computer',computerChoice)

    if(userChoice == computerChoice):
        return "It's a tie! try again"
    
    if isUserWon(userChoice, computerChoice):
        return "You won!"
    
    return "You lose!"
    

def printChoice(type,choice):
    if choice == 'r':
        print(f'{type} choice : rock')
    if choice == 'p':
        print(f'{type} choice : paper')
    if choice == 's':
        print(f'{type} choice : Scissor')

def isUserWon(player, opponent):
    #r>s s>p p>r
    if (player == 'r' and opponent == 's') or (player == 's' and opponent == 'p') \
        or (player == 'p' and opponent == 'r'):
        return True

print(play())

