import urllib.request

def download_jpg(imgurl, filename):
    fullPath = "images/"+filename+".jpg"
    urllib.request.urlretrieve(imgurl, fullPath)

imgPath = input("Enter image URL to download : ")
fileName = input("Enter filename to save as : ")

download_jpg(imgPath,fileName)