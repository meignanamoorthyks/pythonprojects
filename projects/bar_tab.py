class Tab:
    menu = {
        'dosa':15,
        'idly':8,
        'parotta':18,
        'pongal':30,
        'vadai':8,
        'poori':10
    }

    def __init__(self):
        self.items = []
        self.itemCount = {}
        self.totalPerItem = {}
        self.total = 0

    def addItem(self,item,count):
        self.items.append(item)
        self.itemCount[item] = count
        self.totalPerItem[item] = self.menu[item]*count
        self.total += self.menu[item]*count
    
    def printBill(self):
        tax = ( 10/100 ) * self.total
        service = ( 10/100 ) * self.total
        total = self.total + tax + service

        for item in self.items:
            print(f'{item:10} Rs.{self.menu[item]:10} {self.itemCount[item]:10} Rs.{self.totalPerItem[item]}')
        print(f'{"Total":30} Rs.{total:.2f}')