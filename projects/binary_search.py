import random
import time

def native_search(l,target):
    for i in range(len(l)):
        if l[i] == target:
            return i
    return -1

def binary_search(l,target,low=None,high=None):
    if low == None:
        low = 0
    if high == None:
        high = len(l)-1

    midpoint = (low + high) // 2

    if l[midpoint] == target:
        return midpoint
    if l[midpoint] < target:
        return binary_search(l,target,midpoint+1,high)
    if l[midpoint] > target:
        return binary_search(l,target,low,midpoint-1)
    
if __name__ == '__main__':
    bsArr = []
    listLength = int(input("Enter the length of list that you gonna create for binary search: "))
    print(f'Enter {listLength} numbers one by one for the list')
    for i in range(listLength):
        bsArr.append(int(input(f"Enter element number {i+1}: ")))

    print(f"The list created by you: {sorted(bsArr)}")
    target = int(input("Enter the target that you wanna find: "))
    while target not in bsArr:
        target = int(input("Invalid! Enter the target that you wanna find in list: "))

    start = time.time()
    print("Target found in the ", native_search(sorted(bsArr),target)+1, "th position!")
    end = time.time()
    print("Naive search time: ", (end - start), "seconds")

    start = time.time()
    print("Target found in the ", binary_search(sorted(bsArr),target)+1, "th position!")
    end = time.time()
    print("Binary search time: ", (end - start), "seconds")


    