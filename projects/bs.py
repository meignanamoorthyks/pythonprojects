def nativeSearch(arr, target):
    for i in range(len(arr)):
        if arr[i] == target:
            return i


def binarySearch(arr,target,start=None,end=None):
    if start == None:
        start = 0
    if end == None:
        end = len(arr)-1
    
    midPoint = (start + end) // 2

    if target == arr[midPoint]:
        return midPoint
    if target > arr[midPoint]:
        return binarySearch(arr, target,midPoint+1,end)
    if target < arr[midPoint]:
        return binarySearch(arr, target,start,midPoint-1)

if __name__ == "__main__":
    print("Hi, Welcome to Binaray Search")
    bsArr = []
    arrCount = int(input("Enter the array length you wanna create for Binary search : "))
    print("That's Awesome! let's start to enter the element")
    for i in range(arrCount):
        bsArr.append(int(input(f"Enter element {i} : ")))

    bsArr = sorted(bsArr)
    print(f"The array created by you: {bsArr}")
    target = int(input("Enter the target number you want to find in the array : "))

    while target not in bsArr:
        target = int(input("invalid! enter the target number again : "))
    
    #print("The target number is found in position number",nativeSearch(bsArr, target)+1,"!")
    print("Target found in the ", binarySearch(bsArr, target)+1, "th position!")

